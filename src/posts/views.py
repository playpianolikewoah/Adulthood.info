from django.shortcuts import get_object_or_404
from django_rest_logger import log
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.models import User
from accounts.serializers import UserRegistrationSerializer, UserSerializer
from lib.utils import AtomicMixin

from posts.models import Post
from posts.serializers import PostCreationSerializer,PostSerializer

from rest_framework import authentication
from rest_framework import exceptions



class PostCreationView(GenericAPIView):
    serializer_class = PostCreationSerializer
    #authentication_classes = (TokenAuthentication,)
    #permission_classes = (IsAuthenticated,)
    def post(self, request):
        """Post Creation view."""
        #import pdb
        #pdb.set_trace()
        #user= User.objects.get(request.Post['author'])
        #post=Post.objects.latest('date_created')
        serializer = PostCreationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GetAllPostsView(GenericAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    def get(self, request):
        posts = Post.objects.filter(is_question=False).order_by('-date_created')
        serializer = PostSerializer(posts, many=True)
        
        return Response(serializer.data, status=status.HTTP_200_OK)
        

class GetAllQuestionsView(GenericAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    def get(self, request):
        posts = Post.objects.filter(is_question=True).order_by('-date_created')
        serializer = PostSerializer(posts, many=True)
        
        return Response(serializer.data, status=status.HTTP_200_OK)
        
        

#class GetAllPostByCategoryView(GenericAPIView):
 #   serializer_class = PostSerializer
 #   queryset = Post.objects.all()
  #  def get(self, request):
  #      posts = Post.objects.filter(is_question=False,category=self.request.post.category).order_by('-date_created')
  #      serializer = PostSerializer(posts, many=True)
        
   #     return Response(serializer.data, status=status.HTTP_200_OK)
        

