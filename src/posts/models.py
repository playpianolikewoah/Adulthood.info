
from datetime import timedelta

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from accounts.models import User

class Post(models.Model):
    title = models.CharField(max_length = 200)
    author = models.ForeignKey(User, related_name = "user")
    category = models.CharField(max_length = 50)
    content = models.TextField()
    date_created = models.DateTimeField(auto_now = True)
    is_question = models.BooleanField(default = False)

    #def create_Post(self, title, author, content, category, isQuestion):
    #    """
    #    :param title: string
    #    :param author: ForeignKey
    #    :param category: string
    #    :param content: TextField()
    #    :param date_created: DateTime
    #   :param isQuestion: boolean
    #    :return: Post
    #    """
    #    now = timezone.now()
    #    post = self.model(
    #              title=title,
    #              author=User.id,
    #              content=content,
    #              category=category,
    #              date_created = now,
    #              is_question=isQuestion)
    #    post.save(using=self._db)
    #
    #    return post
  
    def __str__(self):
        """
        Unicode representation for an user model.

        :return: string
        """
        return self.title

#class Image(models.Model):
#   image = models.ImageField()
#    postId = models.ForeignKey(Post, related_name ="post")


    
      

