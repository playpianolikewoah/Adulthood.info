from rest_framework import serializers

from accounts.models import User
from posts.models import Post


class PostCreationSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = Post
        fields = ('id', 'title', 'author', 'category', 'content', 'is_question')

        def create(self, validated_data):
          """
          Create the object.

          :param validated_data: string
          """
          return Post.objects.create(**validated_data)

class PostSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Post
        fields = ('title','author','category', 'content','date_created')