import React from 'react'
import { push } from 'react-router-redux'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import './style.scss'
import { Home } from 'components'

class HomeContainer extends React.Component {
  goToProtected = () => {
    this.props.dispatch(push('/protected'))
  };

  render () {
    return (
      <Home userName={this.props.userName} goToProtected={this.goToProtected} />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userName: state.users.authedEmail ? state.users[state.users.authedEmail].first_name : ''
  }
}

HomeContainer.propTypes = {
  userName: PropTypes.string,
  dispatch: PropTypes.func.isRequired
}

export default connect(mapStateToProps)(HomeContainer)
export { HomeContainer as HomeContainerNotConnected }
