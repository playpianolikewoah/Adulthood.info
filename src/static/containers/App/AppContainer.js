import React from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import * as userActionCreators from 'reduxModules/modules/users'
import { bindActionCreators } from 'redux'
// import { authLogoutAndRedirect } from './actions/auth'
import 'resources/styles/main.scss'
import { Navbar } from 'components'
import { checkToken } from 'helpers/utils'

class AppContainer extends React.Component {
  static propTypes = {
    children: PropTypes.shape().isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string
    }),
        // users
    isAuthed: PropTypes.bool.isRequired,
    isFetchingActive: PropTypes.bool.isRequired,
    logoutAndUnauth: PropTypes.func.isRequired

  }

  static defaultProps = {
    location: undefined
  }

  componentWillMount () {
    this.checkAuth()
  }

  checkAuth = () => {
    if (!this.props.isAuthed && !this.props.error && checkToken()) {
             // logging in with token
      this.props.getActiveUser()
    }
  }

  logout = () => {
    this.props.logoutAndUnauth()
        .then(this.goToIndex())
  }

  goToIndex = () => {
    this.props.push('/')
  }

  goToLogin = () => {
    this.props.push('/login')
  }

  goToProtected = () => {
    this.props.push('/protected')
  }

  render () {
    const homeClass = classNames({
      active: this.props.location && this.props.location.pathname === '/'
    })
    const protectedClass = classNames({
      active: this.props.location && this.props.location.pathname === '/protected'
    })
    const loginClass = classNames({
      active: this.props.location && this.props.location.pathname === '/login'
    })

    return (
      <div className='app'>
        <Navbar
          homeClass={homeClass}
          loginClass={loginClass}
          protectedClass={protectedClass}
          goToIndex={this.goToIndex}
          goToProtected={this.goToProtected}
          logout={this.logout}
          goToLogin={this.goToLogin}
          isAuthed={this.props.isAuthed} />
        <div>
          {this.props.isFetchingActive ? <div>Loading...</div> : this.props.children}
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({users, routing}, ownProps) => {
  return {
    isAuthed: users.isAuthed,
    location: routing.location,
    isFetchingActive: users.isFetchingActive,
    error: users.error
  }
}

export default connect(
    mapStateToProps,
    (dispatch) => bindActionCreators(
      {
        ...userActionCreators,
        push
      },
         dispatch
      )
)(AppContainer)
export { AppContainer as AppContainerNotConnected }
