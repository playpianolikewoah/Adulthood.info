// stores user info
import { SERVER_URL } from 'helpers/config'
import { getLocalToken, setLocalToken, unsetLocalToken, checkHttpStatus, parseJSON } from 'helpers/utils'
import { CALL_API, getJSON } from 'redux-api-middleware'
import _ from 'lodash'
// import jwtDecode from 'jwt-decode'

import { REQUEST_POSTS, RECEIVE_POSTS, FAILURE_POSTS, UNAUTH_USER,
  REMOVE_FETCHING_ACTIVE_USER, REQUEST_ACTIVE_USER, RECEIVE_ACTIVE_USER,
  FAILURE_ACTIVE_USER, REQUEST_REGISTER, RECEIVE_REGISTER, FAILURE_REGISTER, FAILURE_ACTIVE_USER_TOKEN } from '../constants'

// Action creators

export function logoutAndUnauth () {
  unsetLocalToken()
  return {
    type: UNAUTH_USER
  }
}

// export function removeFetchingUser () {
//   return {
//     type: REMOVE_FETCHING_USER,
//   }
// }

export function removeFetchingActiveUser () {
  return {
    type: REMOVE_FETCHING_ACTIVE_USER
  }
}

function requestActiveUser () {
  return {
    type: REQUEST_ACTIVE_USER
  }
}

function authLoginUserFailure (status, message) {
  return {
    type: FAILURE_ACTIVE_USER,
    payload: {
      status,
      message
    }
  }
}

function authLoginUserSuccess (token, user) {
  return {
    type: RECEIVE_ACTIVE_USER,
    payload: {
      token,
      user
    }
  }
}

// export function getUser (email) {
//   return {
//     [CALL_API]: {
//       endpoint: `/db/email/${email}`,
//       method: 'GET',
//       headers: {'Authorization': `Bearer ${getLocalToken()}`},
//       types: [
//         REQUEST_USER,
//         {
//           type: RECEIVE_USER,
//           payload: (action, state, res) => {
//             const result = getJSON(res).then((data) => {
//               return data
//             })
//             return result
//           },
//         },
//         FAILURE_USER,
//       ],
//     },
//   }
// }

// export function addUser (name, email, password, avatar = '') {
//   return {
//     [CALL_API]: {
//       endpoint: `/db/users/`,
//       method: 'POST',
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json',
//       },
//       types: [REQUEST_USER, RECEIVE_USER, FAILURE_USER],
//       body: JSON.stringify({
//         name: name,
//         email: email,
//         password: password,
//         avatar: avatar,
//       }),
//     },
//   }
// }
export function loginWithCredentials (email, password) {
  return (dispatch) => {
    dispatch(requestActiveUser())
    const auth = btoa(`${email}:${password}`)
    return fetch(`${SERVER_URL}/api/v1/accounts/login/`, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${auth}`
      }
    })
          .then(checkHttpStatus)
          .then(parseJSON)
          .then((response) => {
            setLocalToken(response.token)
            dispatch(authLoginUserSuccess(response.token, response.user))
          })
          .catch((error) => {
            if (error && typeof error.response !== 'undefined' && error.response.status === 401) {
                  // Invalid authentication credentials
              return error.response.json().then((data) => {
                dispatch(authLoginUserFailure(401, data.non_field_errors[0]))
              })
            } else if (error && typeof error.response !== 'undefined' && error.response.status >= 500) {
                  // Server side error
              dispatch(authLoginUserFailure(500, 'A server error occurred while sending your data!'))
            } else {
                  // Most likely connection issues
              dispatch(authLoginUserFailure('Connection Error', 'An error occurred while sending your data!'))
            }

            return Promise.resolve() // TODO: we need a promise here because of the tests, find a better way
          })
  }
}
export function register (email, password, fname, lname) {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/accounts/register/`,
      method: 'POST',
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        first_name: fname,
        last_name: lname,
        email: email,
        password: password
      }),
      types: [REQUEST_REGISTER, {
        type: RECEIVE_REGISTER,
        payload: (action, state, res) => {
          const result = getJSON(res).then((data) => {
            setLocalToken(data.token)
            return data
          })
          return result
        }
      }, FAILURE_REGISTER]
    }
  }
}
// export function loginWithCredentials (email, password) {
//   const auth = btoa(`${email}:${password}`);
//   return {
//     [CALL_API]: {
//       endpoint: `${SERVER_URL}/api/v1/accounts/login/`,
//       method: 'post',
//       credentials: 'include',
//       headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json',
//         'Authorization': `Basic ${auth}`
//       },
//       body: JSON.stringify({email: email, password: password}),
//       types: [
//         REQUEST_ACTIVE_USER,
//         {
//           type: RECEIVE_ACTIVE_USER,
//           payload: (action, state, res) => {
//             const result = getJSON(res).then((data) => {
//               setLocalToken(data.token)
//               return data
//             })
//             return result
//           },
//         },
//         {
//           type: FAILURE_ACTIVE_USER,
//           meta: (action, state, res) => {
//             console.log(res)
//             if (res) {
//               return {
//                 status: res.status,
//                 statusText: res.statusText
//               };
//             } else {
//               return {
//                 status: 'Network request failed'
//               }
//             }
//           }
//         },
//       ],
//     },
//   }
// }

function fetchActiveUser () {
  return {
    [CALL_API]: {
      endpoint: `${SERVER_URL}/api/v1/accounts/loginWithToken/`,
      method: 'GET',
      credentials: 'include',
      headers: {
        Authorization: `Token ${getLocalToken()}`,
        Accept: 'application/json'},
      types: [REQUEST_ACTIVE_USER, {
        type: RECEIVE_ACTIVE_USER,
        payload: (action, state, res) => {
          const result = getJSON(res).then((data) => {
            // setLocalToken(data.token)
            // console.log('data', data)
            return data
          })
          return result
        }
      }, FAILURE_ACTIVE_USER_TOKEN]
    }
  }
}

export function getActiveUser () {
  return function (dispatch) {
    return Promise.resolve(dispatch(fetchActiveUser()))
  }
}
// initial state

const initialState = {
  isFetching: true,
  isFetchingActive: false,
  error: '',
  isAuthed: false,
  authedEmail: '',
  emails: []
}

// Reducers

export default function users (state = initialState, action) {
  switch (action.type) {
    case UNAUTH_USER :
      return {
        ...state,
        isAuthed: false,
        authedEmail: '',
        statusText: ''
      }
    // case REQUEST_USER:
    //   return {
    //     ...state,
    //     isFetching: true,
    //   }
    case REQUEST_ACTIVE_USER:
    case REQUEST_REGISTER:
      return {
        ...state,
        isFetchingActive: true
      }
    case REMOVE_FETCHING_ACTIVE_USER :
      return {
        ...state,
        isFetchingActive: false
      }
    // case REMOVE_FETCHING_USER :
    //   return {
    //     ...state,
    //     isFetching: false,
    //   }
    case RECEIVE_ACTIVE_USER:
      // console.log('payload', action.payload)
      return action.payload ? !action.payload.user
        ? {
          ...state,
          isFetchingActive: false,
          error: '',
          statusText: `Authentication Error: ${action.payload.status} - ${action.payload.statusText}`
        }
        : {
          ...state,
          isFetchingActive: false,
          error: '',
          [action.payload.user.email]: action.payload.user,
          isAuthed: true,
          authedEmail: action.payload.user.email,
          emails: _.union([action.payload.user.email], state.emails),
          statusText: 'You have been successfully logged in.'
        } : {

        }
    case RECEIVE_REGISTER:
      return action.payload.user ? {
        ...state,
        isFetchingActive: false,
        error: '',
        [action.payload.user.email]: action.payload.user,
        isAuthed: true,
        authedEmail: action.payload.user.email,
        emails: _.union([action.payload.user.email], state.emails),
        statusText: 'You have been successfully logged in.'
      } : {
        ...state,
        isFetchingActive: false
      }
    // case RECEIVE_USER:
    //   return !action.payload.email
    //     ? {
    //       ...state,
    //       isFetching: false,
    //       error: '',
    //     }
    //     : {
    //       ...state,
    //       isFetching: false,
    //       error: '',
    //       [action.payload.email]: action.payload,
    //       emails: _.union([action.payload.email], state.emails),
    //     }
    case RECEIVE_POSTS:
      // console.log(Object.keys(action.payload.entities.users))
      return !action.payload.result
        ? {
          ...state,
          isFetching: false,
          error: ''
        }
        : {
          ...state,
          isFetching: false,
          error: '',
          ...action.payload.entities.users,
          emails: _.union(Object.keys(action.payload.entities.users), state.emails)
        }
    // case FAILURE_USER:
    //   return {
    //     ...state,
    //     isFetching: false,
    //     error: action.payload.message,
    //   }
    case FAILURE_ACTIVE_USER:
      unsetLocalToken()
      return {
        ...state,
        isFetchingActive: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Authentication Error: ${action.payload.status} - ${action.payload.message}`
      }
    case FAILURE_ACTIVE_USER_TOKEN:
      unsetLocalToken()
      return {
        ...state,
        isFetchingActive: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: ''
      }
    case FAILURE_REGISTER:
      unsetLocalToken()
      return {
        ...state,
        isFetchingActive: false,
        error: action.payload.message,
        status: action.payload.status,
        statusText: `Registration Error: ${action.payload.response.email}`
      }
    default :
      return state
  }
}
